import 'package:meta/meta.dart';

class Location{
  final double lat;
  final double lng;
  Location({
    @required this.lat,
    @required this.lng,
  });

  @override
  String toString() {
    return '$lat,$lng';
  }
}