import 'dart:typed_data';

import 'package:google_places_api/responses/place_response.dart';

class PlacePhotoResponse extends PlaceResponse{
  final Uint8List imageByte;
  PlacePhotoResponse(this.imageByte);
}