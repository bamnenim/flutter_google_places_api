import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:google_places_api/core/utills/place_status.dart';
import 'package:google_places_api/requests/query_autocomplete_request.dart';
import 'package:google_places_api/responses/query_autocomplete_response.dart';
import 'package:mockito/mockito.dart';
import 'package:http/http.dart';
import '../fixtures/fixture_reader.dart';

class MockHttpClient extends Mock implements Client{}

void main() {
  MockHttpClient mockHttpClient;

  setUp(() {
    mockHttpClient = MockHttpClient();
  });

  group('test creating place auto complete response',
  () {
    test('should return response obj when it is made from json response', 
    () {
      ///arrange
      var jsonMap = fixture('place_autocomplete_test_data.json');
      var tResponseFromJson = QueryAutocompleteResponse.fromJson(json.decode(jsonMap));
      ///act
      var tResponseFromObject = QueryAutocompleteResponse(
        status: PlaceStatus(status: 'OK'),
        predicts: [null, null, null, null, null]
      );
      ///assert
      expect(tResponseFromJson, tResponseFromObject);
    });

    test('should return error response when it is made from json error response',
    () {
      ///arrange
      var jsonMap = fixture('zero_results.json');
      var tResponseFromJson = QueryAutocompleteResponse.fromJson(json.decode(jsonMap));
      ///act
      var tResponseFromObject = QueryAutocompleteResponse(
        status: PlaceStatus(status: 'ZERO_RESULTS', errorMessage: null),
        predicts: [],
      );
      ///assert
      expect(tResponseFromJson, tResponseFromObject);
    });
  });

  group('test with request', 
  () {
    test('should return place autocomplete response with full result when the call is successful', 
    () async {
      ///arrange
      when(mockHttpClient.get(any))
      .thenAnswer((_) async => Response(fixture('place_autocomplete_test_data.json'), 200));
      ///act
      var response = await QueryAutocompleteRequest(
        key: null, 
        input: null, 
        httpClient: mockHttpClient
      ).call();
      ///assert
      verify(mockHttpClient.get(any));
      expect(response, '');
    });

    test('should return error status when the call is unsuccessful', 
    () async {
      ///arrange
      when(mockHttpClient.get(any))
      .thenAnswer((_) async => Response(fixture('zero_results.json'), 404));
      ///act
      var response = await QueryAutocompleteRequest(
        key: null, 
        input: null, 
        httpClient: mockHttpClient
      ).call();
      ///assert
      verify(mockHttpClient.get(any));
      expect(response, '');
    });
  });

  test('should work normally with null error message', 
  () {
    ///arrange
    
    ///act
    
    ///assert
    
  });
}